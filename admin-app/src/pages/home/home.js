import React from 'react'
import Layout from '../../components/layout/layout'
import { Jumbotron } from 'react-bootstrap'

const Home = () => {
  return (
    <div>
      <Layout>
        <Jumbotron style={{margin: '5rem', backgroundColor: '#fff'}} className="text-center">
          <h1>Welcom to dashboard</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis quisquam illum sunt cupiditate? Repudiandae iusto sapiente adipisci sed pariatur. Doloremque ipsam eum repellendus reiciendis architecto, cum at delectus deserunt blanditiis?</p>
        </Jumbotron>
      </Layout>
    </div>
  )
}

export default Home
