import React from 'react'
import Layout from '../../components/layout/layout'
import Input from '../../components/UI/input/input'
import { Container, Row, Form, Button, Col } from 'react-bootstrap'

const Signin = () => {
  return (
    <Layout>
      <Container>
        <Row style={{ marginTop: '50px' }}>
          <Col md={{span: 6, offset: 3}}>
            <Form>

              <Input
                label="Email address"
                placeholder="Enter email"
                value=""
                type="email"
                onChange={() => {}}
              />

              <Input
                label="Password"
                placeholder="Password"
                value=""
                type="password"
                onChange={() => {}}
              />

              <Button variant="primary" type="submit">
                Submit
              </Button>

            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

export default Signin
