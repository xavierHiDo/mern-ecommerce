import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css';
import home from './pages/home/home'
import signin from './pages/signin/signin'
import signup from './pages/signup/signup'

function App() {
  return (
    // <div className="App">
      <Router>
        <Switch>
          <Route path="/" component={home} exact/>
          <Route path="/signin" component={signin} exact/>
          <Route path="/signup" component={signup} exact/>
        </Switch>
      </Router>
    // </div>
  );
}

export default App;
