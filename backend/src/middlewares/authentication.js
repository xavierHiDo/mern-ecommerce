const jwt = require("jsonwebtoken");

const requireSignin = (req, res, next) => {

  if (req.headers.authorization){
    let token = req.get("authorization").split(" ")[1];
    jwt.verify(token, process.env.JWT_SECRET, (error, decoded) => {
      if (error) {
        return res.status(401).json({
          ok: false,
          error
        });
      }

      req.user = decoded;
    });
  } else{
    return res.status(400).json({
      ok: false,
      message: 'Authorization required'
    });
  }

  next();
};

const verifyUserRole = (req, res, next) => {
  let user = req.user;
  if ((user.role !== "user")) {
    return res.status(400).json({
      ok: false,
      err: {
        message: "Access denied",
      },
    });
  }
  next();
};

const verifyAdminRole = (req, res, next) => {
  let user = req.user;
  if ((user.role !== "admin")) {
    return res.status(400).json({
      ok: false,
      err: {
        message: "Access denied",
      },
    });
  }
  next();
}

module.exports = {
  requireSignin,
  verifyAdminRole,
  verifyUserRole
};
