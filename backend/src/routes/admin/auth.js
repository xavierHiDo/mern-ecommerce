const { Router } = require("express");
const router = Router();

const { signUp, signIn } = require('../../controllers/admin/auth.controller');
const { validateSignupRequest, isRequestValidated, validateSigninRequest } = require("../../validators/auth");

router.post("/admin/signup", [validateSignupRequest, isRequestValidated], signUp);
router.post("/admin/signin", [validateSigninRequest, isRequestValidated], signIn);

module.exports = router;
