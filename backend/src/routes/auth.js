const { Router } = require("express");
const router = Router();
const { signUp, signIn } = require('../controllers/auth.controller');
const { requireSignin } = require('../middlewares/authentication');
const { validateSignupRequest, isRequestValidated, validateSigninRequest } = require("../validators/auth");


router.post("/user/signup", [validateSignupRequest, isRequestValidated], signUp);

router.post("/user/signin", [validateSigninRequest, isRequestValidated], signIn);

router.post("/user/profile", requireSignin, (req, res) =>{
  res.json({
    ok: true
  })
});

module.exports = router;
